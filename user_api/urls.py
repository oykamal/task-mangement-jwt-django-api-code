from . import views

from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
# for implementation of method 3
from . import views

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.authtoken import views as a_views

from rest_framework_simplejwt import views as jwt_views

# urlpatterns = [
#     path('Employee/',views.Employee_list),
#     path('Employee/<int:pk>',views.Employee_details),
# ]

####     method 3

urlpatterns = [

    #for getting the end point 

    # path('api-token-auth/', obtain_auth_token, name='api_token_auth'),  


    #JWT implementation
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),



    # all the listing of the database
    path('employee/',views.EmployeeSareDeka.as_view(),name="list all of the Employee"),
    path('role/',views.RoleList.as_view()),
    path('company/',views.CompanyList.as_view()),
    path('project/',views.ProjectList.as_view()),
    path('project_modules/',views.Project_ModulesList.as_view()),
    
    #creating api end point

    path('employee-create/',views.EmployeeCreate.as_view(),name="list all of the Employee"),
    path('role-create/',views.RoleCreate.as_view()),
    path('company-create/',views.CompanyCreate.as_view()),
    path('project-create/',views.ProjectCreate.as_view()),
    path('project_modules-create/',views.Project_ModulesCreate.as_view()),


    #deleting  specific api end point

    path('employee-delete/<int:pk>',views.EmployeeDelete.as_view(),name="list all of the Employee"),
    path('role-delete/<int:pk>',views.RoleDelete.as_view()),
    path('company-delete/<int:pk>',views.CompanyDelete.as_view()),
    path('project-delete/<int:pk>',views.ProjectDelete.as_view()),
    path('project_modules-delete/<int:pk>',views.Project_ModulesDelete.as_view()),


    #updating specific model api end point 

    path('employee-update/<int:pk>',views.EmployeeUpdate.as_view(),name="list all of the Employee"),
    path('role-update/<int:pk>',views.RoleUpdate.as_view()),
    path('company-update/<int:pk>',views.CompanyUpdate.as_view()),
    path('project-update/<int:pk>',views.ProjectUpdate.as_view()),
    path('project_modules-update/<int:pk>',views.Project_ModulesUpdate.as_view()),


    #retrieve specific model api end point 

    path('employee-retrieve/<int:pk>',views.EmployeeRetrieve.as_view(),name="list all of the Employee"),
    path('role-retrieve/<int:pk>',views.RoleRetrieve.as_view()),
    path('company-retrieve/<int:pk>',views.CompanyRetrieve.as_view()),
    path('project-retrieve/<int:pk>',views.ProjectRetrieve.as_view()),
    path('project_modules-retrieve/<int:pk>',views.Project_ModulesRetrieve.as_view()),



    # path('',include('djoser.urls')),
    # path('',include('djoser.urls.authtoken'))


]



urlpatterns= format_suffix_patterns(urlpatterns)