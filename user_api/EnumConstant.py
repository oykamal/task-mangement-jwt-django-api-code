from enum import Enum


class Gender(Enum):   # A subclass of Enum
    M = "Male"
    F = "Female"
