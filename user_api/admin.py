from django.contrib import admin

from .models import Employee, Company, Project, Project_Modules, Role
# Register your models here.

admin.site.register(Employee)
admin.site.register(Company)
admin.site.register(Project)
admin.site.register(Project_Modules)
admin.site.register(Role)